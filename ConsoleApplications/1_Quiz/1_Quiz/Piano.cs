﻿using System;

namespace _1_Quiz
{
    class Piano {
        public Piano() { 
            while (true) {
                char key = Console.ReadKey(false).KeyChar;

                //Piano keys
                if (key == 'a')
                    Console.Beep(261, 300);
                if (key == 's')
                    Console.Beep(293, 300);
                if (key == 'd')
                    Console.Beep(329, 300);
                if (key == 'f')
                    Console.Beep(349, 300);
                if (key == 'g')
                    Console.Beep(392, 300);
                if (key == 'h')
                    Console.Beep(440, 300);
                if (key == 'j')
                    Console.Beep(493, 300);
                if (key == 'k')
                    Console.Beep(523, 300);

                if (key == 'q') break;
            }
        }
    }
}
