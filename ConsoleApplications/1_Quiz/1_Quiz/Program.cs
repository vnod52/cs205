﻿using System;
using System.Collections.Generic;

namespace _1_Quiz {
    class Program {
        static void Main() {
            new Question(
                "Shall we play?"
            ,   new Dictionary<string, Action> {
                {"yes", () => {

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("\nWoo! Let's play! Press ASKFGHJK!!!");

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Press Q to quit.\n");
                        Console.ResetColor();

                        new Piano();

                    }
                }, { "NO", () => {
                        Console.WriteLine("Aughhhhh!");

                        new Question(
                            "You sure about that????"
                            , new Dictionary<string, Action> {
                                    { "mm-hmmm", () => {
                                        Console.WriteLine("Fine :'(");
                                    }
                                }
                            }
                            );
                    }
                }
                }
            );

            Console.Beep(200,100);
            Console.Beep(160, 200);
            Console.WriteLine("\n Thanks for playing! Press any key to close");

            Console.ReadKey(true);

            //while (!Console.KeyAvailable) { }

        }
    }
}
