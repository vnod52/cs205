﻿
using System;

namespace TopDown {
    class Player {
        public Player(Vector2 _position) {
            position = _position;
            maxHp = hp;
        }

        public int hp = 10;
        public static Vector2 position;

        private string log = "";
        private int maxHp;
        private int potions;


        public void Act() {
            log = "";

            if (Game.Instance.info.Key == ConsoleKey.I) {
                UsePotion();
            }

            Vector2 direction = GetInputDirection();

            if (!isObstructed(direction)) {
                position += direction;
            }
        }

        public void Log() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("hp: " + hp + "\t\tpotions: " + potions);
            Console.ResetColor();
            Console.Write("\nWASD = Move \tI=Use Potion");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\npos: ()" + position.ToString()) ;

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write(log);

        }

        private bool CollideCactus() {
            log += "\nYou walked into a cactus and lose 2hp.";
            hp -= 2;
            if (hp < 0) {
                hp = 0;
            }
            return false;
        }

        private bool CollidePotion(ref char _c) {
            log += "\nYou found a potion.";
            potions++;
            _c = Game.Instance.GetRandomGroundChar();
            return false;
        }

        private bool CollideEnemy() {
            log += "\nAn enemy blocks your way";
            return true;
        }

        private bool CollideRock() {
            log += "\nA rock blocks your way";
            return true;
        }

        private Vector2 GetInputDirection() {
            if (Game.Instance.info.Key == ConsoleKey.W) return Vector2.Down;
            if (Game.Instance.info.Key == ConsoleKey.S) return Vector2.Up;
            if (Game.Instance.info.Key == ConsoleKey.A) return Vector2.Left;
            if (Game.Instance.info.Key == ConsoleKey.D) return Vector2.Right;

            return Vector2.Zero;
        }

        private bool HandleObstruction(ref char _c) {
            switch (_c) {
                case '▲':
                    return CollideRock();
                case '♣':
                    return CollideCactus();
                case '§':
                    return CollideEnemy();
                case 'Φ':
                    return CollidePotion(ref _c);
                default:
                    return true;
            }
        }

        private bool isObstructed(Vector2 _direction) {
            Vector2 nextPosition = position + _direction;
            if (nextPosition.x > Game.Instance.Map.GetLength(0) - 1
            || nextPosition.y > Game.Instance.Map.GetLength(1) - 1
            || nextPosition.x < 0
            || nextPosition.y < 0
            ) {
                log += "\nYou can go no further.";
                return true;
            }
            string terrainChars = " \".,'`\'";
            bool isObjectTile = false;
            foreach (char c in terrainChars) {
                if (!terrainChars.Contains(Game.Instance.Map[nextPosition.x, nextPosition.y].ToString())) {
                    isObjectTile = true;
                }
            }
            if (isObjectTile) {
                isObjectTile = HandleObstruction(ref Game.Instance.Map[nextPosition.x, nextPosition.y]);
            }
            return isObjectTile; 
        }

        private void UsePotion() {
            if (potions > 0) {
                if (hp < maxHp) {
                    log = "\nYou used a potion, recovering 5hp.";
                    potions--;
                    hp += 5;

                    if (hp > maxHp) {
                        hp = maxHp;
                    }
                } else {
                    log = "\nYou don't need to use a potion right now.";

                }
            } else {
                log = "\nYou don't have any potions";

            }
        }



    }
}
