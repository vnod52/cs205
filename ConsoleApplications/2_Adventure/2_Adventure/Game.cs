﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace _2_Adventure {
    class Game {
        public const string TITLE = "Final Adventure";

        public static Player PlayerCharacter;

        public static void Play() {
            Console.Title = TITLE;

            Console.WriteLine(@"______ _             _    ___      _                 _");
            Console.WriteLine(@"|  ___(_)           | |  / _ \    | |               | |");
            Console.WriteLine(@"| |_   _ _ __   __ _| | / /_\ \ __| |_   _____ _ __ | |_ _   _ _ __ ___");
            Console.WriteLine(@"|  _| | | '_ \ / _` | | |  _  |/ _` \ \ / / _ \ '_ \| __| | | | '__/ _ \");
            Console.WriteLine(@"| |   | | | | | (_| | | | | | | (_| |\ V /  __/ | | | |_| |_| | | |  __/");
            Console.WriteLine(@"\_|   |_|_| |_|\__,_|_| \_| |_/\__,_| \_/ \___|_| |_|\__|\__,_|_|  \___|");
            Console.WriteLine("\nBy Steve Enox");

            Console.WriteLine("\nWelcome to " + TITLE + ", a tale in which magic & wonder await you.");
            WriteLineStandard("Press any key to read on...");

            WriteLineStandard("You awaken to the surprise of wet drips on your face.");
            WriteLineStandard("Water? No... beer, you realize as it rolls down your cheek. Your tongue acts without you, licking your otherwise dry lips.");
            WriteLineStandard("More drizzles from the grimy table under which you're lying, tummy-down, face to one-side, sprawled and bruised in what seems to be a closed bar in the after-hours.");
            WriteLineStandard("You don't drink booze, how did you get here?");
            WriteLineStandard("Footsteps approach from behind, circling around and revealing a slender silhouette before you.");
            WriteLineStandard("You strain to look up, but quickly realize you're in no shape to move.");
            WriteLineStandard("The figure kneels with one arm to the ground, leaning to look you in the eye.");
            WriteLineStandard("You can barely discern an elderly woman's malevolent stare in the pale neon-glow from the bar. You feel her ragged hair weighing over your arms.");

            WriteLineMultiFormat(new ColoredText[]{
                new ColoredText("\"You're not what I ordered...\"", ConsoleColor.Cyan)
            ,   new ColoredText(" she croaks through seasoned chords.")
            });

            WriteLineStandard("You're not seeing straight, you feel dizzy... faint... you panic to place a response, any will do...");

            WriteLineMultiFormat(new ColoredText[]{
                new ColoredText("Your throat is dry, you rasp: ")
            ,   new ColoredText("\n\"My... n... name... ... is...\"", ConsoleColor.Cyan)
            }, false, false);

            string playerName = "";

            Prompt(
                "Enter your name: "
            , ref playerName
            );
            ConvertToName(ref playerName);

            WriteLineMultiFormat(new ColoredText[] {
                new ColoredText("\"", ConsoleColor.Cyan)
            ,   new ColoredText(playerName, ConsoleColor.Green)
            ,   new ColoredText("? Not like I asked...\"", ConsoleColor.Cyan)
            ,   new ColoredText(" she pauses for a moment and sighs as she closes her eyes, lighting a cigarette.")
            });

            WriteLineMultiFormat(new ColoredText[]{
                new ColoredText("She took a moment, releasing a thought from only a moment ago.")
            ,   new ColoredText(" \"Know what? You'll do...\"", ConsoleColor.Cyan)
            });

            WriteLineStandard("With a cough she spreads smoke everywhere & catches a glob of mucus in her hand.");
            WriteLineStandard("Gross! She scrapes it into an already half-filled, blue vial from her pocket.");
            WriteLineStandard("Sealing it with her thumb, she mixes it then pours it over your face. You're still in no shape to move.");
            WriteLineStandard("You vomit a little... to your surprise, her mucus dries to a pile of fine powder that collapses on your skin.");
            WriteLineStandard("Strange, it must have been a healing tonic, you feel yourself able to move again. You stand up to face this strange, old woman.");

            WriteLineMultiFormat(new ColoredText[]{
                new ColoredText("\"You can call me 'Boss'\"", ConsoleColor.Cyan)
            ,   new ColoredText(" Your new boss seems disappointed... ")
            ,   new ColoredText("\"I got weapons, you'd better get good at one of these\".", ConsoleColor.Cyan)
            });

            PresentOptions("What are you best with?", new Dictionary<string, Action>() {
                    { "Sword", () => {
                        PlayerCharacter = new Player(playerName, Weapon.WeaponType.SWORD);
                }}, { "Knife", () => {
                        PlayerCharacter = new Player(playerName, Weapon.WeaponType.KNIFE);
                }}, { "Staff", () => {
                        PlayerCharacter = new Player(playerName, Weapon.WeaponType.STAFF);
                }}
            });

            string weaponType = PlayerCharacter.weaponType.ToString();
            ConvertToName(ref weaponType);
            WriteLineStandard("\nNo sooner had you picked up your " + weaponType + " than a horde of bandersnatches leapt in through the window.");

            WriteLineStandard("Boss was taking care of most of them, but you found yourself face-to-face with one who decided you might taste better.");
            WriteLineStandard("You draw your " + weaponType + " and get ready to take it down!");

            if (!new Battle(EnemyDatabase.Enemies["Bandersnatch"]).Fight())
            {
                return;
            }

            Console.WriteLine();

            WriteLineStandard("You shove your " + weaponType + " into the beast's eyeballs but it doesn't care, it snaps its own skull off to begin attacking you with it's bleeding, severed neck.");
            WriteLineStandard("Boss pushes you aside and fries it with her laser-eyes! Yay, the day is saved. You collapse from exhaustion.");
        }

        public static void PresentOptions(string _question, Dictionary<string, Action> _outcomes) {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(_question);

            Console.ForegroundColor = ConsoleColor.Green;
            foreach (KeyValuePair<string, Action> kvp in _outcomes) {
                Console.WriteLine("- " + kvp.Key);
            }

            Console.ResetColor();
            Console.WriteLine();

            string response;
            do {
                response = Console.ReadLine();
            } while (!_outcomes.ContainsKey(response));

            Console.WriteLine();

            _outcomes[response]();
        }

        public static void Prompt(string _promptText, ref string _string) {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(_promptText);
            Console.ResetColor();

            do {
                _string = Console.ReadLine();
            } while (string.IsNullOrEmpty(_string));

            Console.Write("\n");
        }

        public static void WriteStandard(string _text, ConsoleColor color = ConsoleColor.Gray, bool isWaitAfter = true, int typeDelay = 1) {
            Console.ForegroundColor = color;

            foreach (char c in _text) {
                Console.Write(c);
                Thread.Sleep(typeDelay);
            }

            Console.ResetColor();

            if (isWaitAfter) {
                Console.ReadKey(true);
            }
        }

        public static void WriteLineStandard(string _text, ConsoleColor _color = ConsoleColor.Gray, bool _isAddSpace = true) {
            WriteStandard(_text, _color);

            if (_isAddSpace) {
                Console.WriteLine("\n");
            }
        }

        public static void WriteLineMultiFormat(ColoredText[] _texts, bool _isAddSpace = true, bool isWaitAfter = true) {
            foreach (ColoredText ct in _texts) {
                WriteStandard(ct.text, ct.consoleColor, false);
            }

            if (_isAddSpace) {
                Console.WriteLine("\n");
            } else {
                Console.Write("\n");
            }

            Console.ResetColor();

            if (isWaitAfter) {
                Console.ReadKey(true);
            }
        }

        public static void ConvertToName(ref string _text) {
            _text = Regex.Match(_text, @"[a-zA-Z]+").Value.ToLower();
            char[] charArray = _text.ToCharArray();
            charArray[0] = (char)(charArray[0] - 32);
            _text = new string(charArray);
        }
    }
}
