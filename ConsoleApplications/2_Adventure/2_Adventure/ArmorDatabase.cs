﻿using System.Collections.Generic;

namespace _2_Adventure {
    class ArmorDatabase {
        public static Dictionary<string, Armor> Armor = new Dictionary<string, Armor>() {
            { "Heavy Cloth"  , new Armor("Heavy Cloth"  , 8,  6,  4,  10) }
        ,   { "Leather Plate", new Armor("Leather Plate", 20, 9,  9,  8) }
        ,   { "Kevlar Vest"  , new Armor("Kevlar Vest"  , 28, 14, 15, 4) }
        };
    }
}
