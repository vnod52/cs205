﻿using System;

namespace _2_Adventure {
    public struct Weapon {
        public Weapon(string _name, WeaponType _type, int _mp, int _strength, int _intelligence, int _accuracy) {
            name = _name;
            weaponType = _type;
            mp = _mp;
            strength = _strength;
            intelligence = _intelligence;
            accuracy = _accuracy;
        }

        public enum WeaponType {
            SWORD
        , KNIFE
        , STAFF
        }

        public string name;
        public WeaponType weaponType;
        public int mp;
        public int strength;
        public int intelligence;
        public int accuracy;
    }
}
