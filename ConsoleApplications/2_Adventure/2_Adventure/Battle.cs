﻿using System;

namespace _2_Adventure {
    class Battle {
        public Battle(Enemy _enemy) {
            enemy = _enemy;
        }

        private Enemy enemy;
        private int round = 0;

        public bool Fight() {
            Game.WriteStandard("A battle has begun! " + enemy.name + " appears.\n", ConsoleColor.Magenta, true, 50);

            while (true) {
                if (Game.PlayerCharacter.hp < 1) {
                    Game.WriteStandard("\n\nYou died. Game Over...");
                    return false;
                }

                if (enemy.hp < 1) {
                    Victory();
                    return true;
                }

                Character a = new Random().Next(0, 2) == 0 ? (Character)Game.PlayerCharacter : enemy;
                Character b = a == Game.PlayerCharacter ? enemy : (Character)Game.PlayerCharacter;

                ReportRoundData(a);

                Ability playerAbility = Game.PlayerCharacter.PresentOptions();

                EvaluateDamage(a, b, playerAbility);
                if (b.hp < 1) {
                    continue;
                }
                Console.WriteLine();
                EvaluateDamage(b, a, playerAbility);
                Console.WriteLine();
            }
        }

        private void Victory() {
            Game.WriteStandard("\nYou won!", ConsoleColor.Yellow, false);
            Game.WriteStandard("\nYou earned " + enemy.exp + "exp", ConsoleColor.Yellow, false);

            if (enemy.qtyPotions > 0) {
                Game.WriteStandard("\nYou found " + ((enemy.qtyPotions > 1) ? (enemy.qtyPotions + " potions") : "a potion."), ConsoleColor.Yellow, false);
            }

            Console.WriteLine();

            Game.PlayerCharacter.exp += enemy.exp;

            while (Game.PlayerCharacter.exp > Game.PlayerCharacter.expToNextLevel) {
                Game.PlayerCharacter.LevelUp();
            }
        }

        private void ReportRoundData(Character _firstActor) {
            Game.WriteStandard("\nRound " + (++round) + "\n", ConsoleColor.Yellow, false);

            Game.WriteLineMultiFormat(new ColoredText[] {
                new ColoredText(Game.PlayerCharacter.name + GetNameSpacing(Game.PlayerCharacter.name, enemy.name), ConsoleColor.Green)
            ,   new ColoredText(" (hp: " + Game.PlayerCharacter.hp + "\tmp: " + Game.PlayerCharacter.mp + ")" , ConsoleColor.Yellow)
            }, false, false);

            Game.WriteLineMultiFormat(new ColoredText[] {
                new ColoredText(enemy.name + GetNameSpacing(enemy.name, Game.PlayerCharacter.name), ConsoleColor.Red)
            ,   new ColoredText(" (hp: " + enemy.hp + "\tmp: " + enemy.mp + ")" , ConsoleColor.Yellow)
            }, false, false);


            ReportFirstAttacker(_firstActor);

            string GetNameSpacing(string _a, string _b) {
                if (_a.Length > _b.Length) {
                    return "";
                }

                int difference = Math.Abs(Game.PlayerCharacter.name.Length - enemy.name.Length);

                string spaces = "";

                for (int i = 0; i < difference; i++) {
                    spaces += ' ';
                }

                return spaces;
            }
        }

        private static void ReportFirstAttacker(Character a) {
            ConsoleColor characterColor = a.GetType() == typeof(Player) ? ConsoleColor.Green : ConsoleColor.Red;
            Game.WriteLineMultiFormat(new ColoredText[]{
                new ColoredText(a.name, characterColor)
            ,   new ColoredText(" goes first. What will you do?", ConsoleColor.Gray)
            }, true, false);
        }


        private void EvaluateDamage(Character _a, Character _b, Ability _playerAbility) {
            if (_a.GetType() == typeof(Player)) {
                _playerAbility.action(_b, _playerAbility.power);
            } else {
                ((Enemy)_a).Attack(_b);
            }
        }
    }
}
