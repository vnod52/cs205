﻿using System;
using System.Collections.Generic;

namespace _2_Adventure {
    class AbilityDatabase {
        public static Dictionary<string, Ability> Standard = new Dictionary<string, Ability> {
            { "Attack", new Ability("Attack", 0, 1, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.strength, _multiplier);
            })}
            , { "Potion", new Ability("Potion", 0, 0, (_target, _multiplier) => {
                Game.PlayerCharacter.hp += 100;
                if (Game.PlayerCharacter.hp > Game.PlayerCharacter.maxHP) {
                    Game.PlayerCharacter.hp = Game.PlayerCharacter.maxHP;
                }

                Game.PlayerCharacter.qtyPotions--;
                Game.WriteStandard(Game.PlayerCharacter.name + " uses a potion, recovering 100hp.", ConsoleColor.Green, false);
            })}
        };

        public static Dictionary<string, Ability> Sword = new Dictionary<string, Ability> {
            { "Downward Slice", new Ability("Downward Slice", 3, 2, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.strength, _multiplier);
            })}
            , { "Slish-Slash", new Ability("Slish-Slash", 5, 4, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.strength, _multiplier / 3);
                AttackStandard(_target, Game.PlayerCharacter.strength, _multiplier / 3);
            })}
            , { "Sharp Gust", new Ability("Sharp Gust", 8, 6, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.strength, _multiplier);
            })}
        };

        public static Dictionary<string, Ability> Knife = new Dictionary<string, Ability> {
            { "Shank", new Ability("Shank", 3, 2, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.accuracy, _multiplier);
            })}
            , { "Steal", new Ability("Steal", 5, 0, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.accuracy, _multiplier);
            })}
            , { "Triple-Stab", new Ability("Triple-Stab", 10, 1.5f, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.accuracy, _multiplier / 3);
                AttackStandard(_target, Game.PlayerCharacter.accuracy, _multiplier / 3);
                AttackStandard(_target, Game.PlayerCharacter.accuracy, _multiplier / 3);
            })}
        };

        public static Dictionary<string, Ability> Staff = new Dictionary<string, Ability> {
            { "Fire", new Ability("Fire", 5, 4, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.intelligence, _multiplier);
            })}
            , { "Ice", new Ability("Ice", 5, 4, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.intelligence, _multiplier);
            })}
            , { "Bolt", new Ability("Bolt", 5, 4, (_target, _multiplier) => {
                AttackStandard(_target, Game.PlayerCharacter.intelligence, _multiplier);
            })}
        };

        public static void AttackStandard(Character _target, int _modifier, float _multiplier) { }

        public static bool IsHit(Character _user, Character _target) {
            return true;
        }
    }
}
