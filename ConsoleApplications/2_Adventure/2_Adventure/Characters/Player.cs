﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _2_Adventure {
    class Player : Character {
        public Player(string _name, Weapon.WeaponType _weaponType) : base(_name) {
            weaponType = _weaponType;
            Dictionary<string, Ability> dictClassAbilities = new Dictionary<string, Ability>(AbilityDatabase.Sword);
            
            switch (weaponType)
            {
                case Weapon.WeaponType.SWORD:
                    SetStats(320, 34, 22, 20, 11, 13, 18, 16, 14);
                    dictClassAbilities = new Dictionary<string, Ability>(AbilityDatabase.Sword);
                    break;
                case Weapon.WeaponType.KNIFE:
                    SetStats(250, 40, 15, 15, 11, 13, 22, 21, 22);
                    dictClassAbilities = new Dictionary<string, Ability>(AbilityDatabase.Knife);
                    break;
                case Weapon.WeaponType.STAFF:
                    SetStats(190, 95, 11, 12, 22, 23, 17, 15, 14);
                    dictClassAbilities = new Dictionary<string, Ability>(AbilityDatabase.Staff);
                    break;
            }

            abilities = new Dictionary<string, Ability>(AbilityDatabase.Standard.Concat(dictClassAbilities).ToDictionary(kvp => kvp.Key, kvp => kvp.Value));

            growth = new Growth(this);
            weapon = SetDefaultWeapon(_weaponType);
            armor = SetDefaultArmor();
            qtyPotions = 2;
        }
        
        public Weapon.WeaponType weaponType;
        public Weapon weapon;
        public Armor armor;
        public int expToNextLevel = 200;

        private int level = 1;
        private Growth growth;
        private Dictionary<string, Ability> abilities;

        private Weapon SetDefaultWeapon(Weapon.WeaponType _weaponType) {
            Dictionary<string, Weapon> dictWeapons = WeaponDatabase.Swords;

            switch (_weaponType) {
                case Weapon.WeaponType.KNIFE:
                    dictWeapons = WeaponDatabase.Knives;
                    break;
                case Weapon.WeaponType.STAFF:
                    dictWeapons = WeaponDatabase.Staves;
                    break;
            }

            weaponType = _weaponType;
            KeyValuePair<string, Weapon> kvp = dictWeapons.First();

            Game.WriteStandard("You received: " + kvp.Key + "\n", ConsoleColor.Yellow, false);

            Weapon defaultWeapon = kvp.Value;

            return defaultWeapon;
        }

        private Armor SetDefaultArmor() {
            KeyValuePair<string, Armor> kvp = ArmorDatabase.Armor.First();

            Game.WriteStandard("You received: " + kvp.Key + "\n", ConsoleColor.Yellow, false);

            return kvp.Value;
        }

        public Ability PresentOptions() {
            string availableOptions = "";
            KeyValuePair<string, Ability>[] kvp = abilities.ToArray();
            for (int i = 0; i < abilities.Count; i++) {
                if (i > 0) {
                    availableOptions += i == 2 ? "\n" : " | ";
                }

                availableOptions += kvp[i].Key;
                if (kvp[i].Value.mpCost > 0) {
                    availableOptions += " (" + kvp[i].Value.mpCost + "mp)";
                }

                if (kvp[i].Key == "Potion") {
                    availableOptions += " (" + qtyPotions + ")";
                }
            }

            Game.WriteStandard(availableOptions + "\n", ConsoleColor.Green, false);

            Console.ResetColor();

            string response = "";
            do {
                Game.Prompt("Choose a command: ", ref response);
                if (abilities.ContainsKey(response)) {
                    if (mp < abilities[response].mpCost) {
                        Game.WriteStandard("Not enough MP, try something else.\n", ConsoleColor.Gray, false);
                        response = "";
                        continue;
                    } else if (response == "Potion" && qtyPotions < 1) {
                        Game.WriteStandard("You're out of potions, try something else.\n", ConsoleColor.Gray, false);
                        response = "";
                        continue;
                    } else {
                        mp -= abilities[response].mpCost;
                    }
                }
            } while (!abilities.ContainsKey(response));

            return abilities[response];
        }

        public void LevelUp() {
            expToNextLevel = (int)(expToNextLevel * 1.4f);

            maxHP = (int)(maxHP * growth.hp);
            maxMP = (int)(maxMP * growth.mp);
            strength = (int)(strength * growth.strength);
            defense = (int)(defense * growth.defense);
            intelligence = (int)(intelligence * growth.intelligence);
            spirit = (int)(spirit * growth.spirit);
            accuracy = (int)(accuracy * growth.accuracy);
            evasion = (int)(evasion * growth.evasion);
            luck = (int)(luck * growth.luck);

            Game.WriteStandard("\nYou grew to level: " + (++level), ConsoleColor.Yellow, false);
            Console.WriteLine();
            Game.WriteStandard("\nhp: " + maxHP, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\nmp: " + maxMP, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\nstrength: " + strength, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\ndefense: " + defense, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\nintelligence: " + intelligence, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\nspirit: " + spirit, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\naccuracy: " + accuracy, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\nevasion: " + evasion, ConsoleColor.DarkYellow, false);
            Game.WriteStandard("\nluck: " + luck, ConsoleColor.DarkYellow, false);

            Console.ReadKey(true);
            Console.WriteLine();
        }
    }
}
