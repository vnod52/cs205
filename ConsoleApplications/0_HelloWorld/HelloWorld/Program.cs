﻿using System;

namespace HelloWorld
{
    class Program
    {
        public static bool isRunning;
        static void Main(string[] args)
        {
            isRunning = true;

            Console.WriteLine("Hello World!");

            while (isRunning)
            {
                char k = Console.ReadKey(true).KeyChar;
                Console.WriteLine(k);

                if (k == 'q')
                {
                    isRunning = false;
                }
                
            }

        }
    }
}
