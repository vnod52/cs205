﻿using System;

namespace Connect4 {
    public struct Vector2 {
        public Vector2(int _x, int _y) {
            x = _x;
            y = _y;
        }

        public static Vector2 Down  = new Vector2(0,-1);
        public static Vector2 Left  = new Vector2(-1,0);
        public static Vector2 One   = new Vector2(1,1);
        public static Vector2 Right = new Vector2(1,0);
        public static Vector2 Up    = new Vector2(0,1);
        public static Vector2 Zero  = new Vector2(0,0);

        public int x;
        public int y;

        //Arithmatic operators
        public static Vector2 operator +(Vector2 _a, Vector2 _b) => new Vector2(_a.x + _b.x, _a.y + _b.y);
        public static Vector2 operator -(Vector2 _a, Vector2 _b) => new Vector2(_a.x - _b.x, _a.y - _b.y);
        public static Vector2 operator *(Vector2 _a, Vector2 _b) => new Vector2(_a.x * _b.x, _a.y * _b.y);
        public static Vector2 operator *(Vector2 _a, int _b) => new Vector2(_a.x * _b, _a.y * _b);
        public static Vector2 operator /(Vector2 _a, Vector2 _b) => new Vector2(((int)(float)_a.x / _b.x), ((int)(float)_a.y / _b.y));
        public static Vector2 operator /(Vector2 _a, int _b) => new Vector2(((int)(float)_a.x / _b), ((int)(float)_a.y / _b));

        //Logical operators
        public static bool operator ==(Vector2 _a, Vector2 _b) => _a.x == _b.x && _a.y == _b.y;
        public static bool operator !=(Vector2 _a, Vector2 _b) => !(_a.x == _b.x && _a.y == _b.y);
        public static bool operator <(Vector2 _a, Vector2 _b) => _a.x < _b.x | _a.y < _b.y;
        public static bool operator >(Vector2 _a, Vector2 _b) => _a.x > _b.x | _a.y > _b.y;
        public static bool operator <=(Vector2 _a, Vector2 _b) => _a.x <= _b.x | _a.y <= _b.y;
        public static bool operator >=(Vector2 _a, Vector2 _b) => _a.x < _b.x | _a.y >= _b.y;

        public override bool Equals(object _o) => Equals(_o);
        public override int GetHashCode() => GetHashCode();

    }
}
