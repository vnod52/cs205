﻿
using System;

namespace BullsAndCows {
    class Program {

        static void Main(string[] args) {
            //Displays Main title and allow user to select a game option
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.SoundLocation = "media/FF1.wav";
            player.PlayLooping();
            //Console.SetWindowSize(45, 45);
            Console.Title = "Bulls and Cows";
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("||╔╗ ╦ ╦╦  ╦  ╔═╗  ╔═╗╔╗╔╔╦╗  ╔═╗╔═╗╦ ╦╔═╗||");
            Console.WriteLine("||╠╩╗║ ║║  ║  ╚═╗  ╠═╣║║║ ║║  ║  ║ ║║║║╚═╗||");
            Console.WriteLine("||╚═╝╚═╝╩═╝╩═╝╚═╝  ╩ ╩╝╚╝═╩╝  ╚═╝╚═╝╚╩╝╚═╝||");
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.WriteLine("||             SELECT A GAME              ||");
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.WriteLine("||      [1]: Game one - 3 Digit Code      ||");
            Console.WriteLine("||      [2]: Game two - 4 Digit Code      ||");
            Console.WriteLine("||      [3]: Game three - 5 Digit Code    ||");
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.CursorVisible = false;
            Console.WriteLine(" Press corressponding number to start game:\n");

            displayGameOptions();
        }

        private static void displayGameOptions() { //display options for the user to choose
            ConsoleKey userResponse = Console.ReadKey(true).Key;
            if (userResponse == ConsoleKey.NumPad1 || userResponse == ConsoleKey.D1) {
                Console.WriteLine("You have selected 3 digit - Bulls and Cows Game!");
                Console.WriteLine("Press any key continue...");
                Console.ReadKey();
                Console.Clear();
                playThreeDG();
            } else if (userResponse == ConsoleKey.NumPad2 || userResponse == ConsoleKey.D2) {
                Console.WriteLine("You have selected 4 digit - Bulls and Cows Game!");
                Console.WriteLine("Press any key continue...");
                Console.ReadKey();
                Console.Clear();
                playFourDG();
            } else if (userResponse == ConsoleKey.NumPad3 || userResponse == ConsoleKey.D3) {
                Console.WriteLine("You have selected 5 digit - Bulls and Cows Game!");
                Console.WriteLine("Press any key continue...");
                Console.ReadKey();
                Console.Clear();
                playFiveDG();
            } else if (!(userResponse == ConsoleKey.NumPad1 && userResponse == ConsoleKey.D1 
                && userResponse == ConsoleKey.NumPad2 && userResponse == ConsoleKey.D2
                && userResponse == ConsoleKey.NumPad3 && userResponse == ConsoleKey.D3)) {
                Console.ForegroundColor = ConsoleColor.Red; //if any other key press, display error msg
                Console.WriteLine("Invalid key, Please use 1 - 3 on keyboard");
                Console.ResetColor();
                displayGameOptions();
            }
        }

        private static void playThreeDG() { // play 3 digit code guess game
            ThreeDigitGame gameOne = new ThreeDigitGame();
            gameOne.DisplayTitle();
            gameOne.generateSecretNum();

            while (gameOne.notGameOver) {
                gameOne.getUserInput();
            }
        }

        private static void playFourDG() { // play 4 digit code guess game
            FourDigitGame gameTwo = new FourDigitGame();
            gameTwo.DisplayTitle();
            gameTwo.generateSecretNum();

            while (gameTwo.notGameOver) {
                gameTwo.getUserInput();
            }

        }
        private static void playFiveDG() { // play 5 digit code guess game
            FiveDigitGame gameThree = new FiveDigitGame();
            gameThree.DisplayTitle();
            gameThree.generateSecretNum();

            while (gameThree.notGameOver) {
                gameThree.getUserInput();
            }

        }


    }
}
