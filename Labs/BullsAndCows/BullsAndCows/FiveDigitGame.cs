﻿
using System;

namespace BullsAndCows {
    class FiveDigitGame {
        //User guess, numbers user increment for each code
        int A = 0;
        int B = 0;
        int C = 0;
        int D = 0;
        int E = 0;

        //Line number to display duplicates message
        int lineNum = 15;

        //Variables to calculate bulls and cows in game
        int bullA, bullB, bullC, bullD, bullE;
        int cowA, cowB, cowC, cowD, cowE;
        int Bulls = 0;
        int Cows = 0;

        public bool notGameOver = true;
        
        //Random num generator and array to store secret numbers.
        Random randNum = new Random();
        int[] secNum = new int[5];

        public void DisplayTitle() { //Displays game title and instructions
            Console.Title = "Bulls and Cows";
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.WriteLine("||             BULLS AND COWS             ||");
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("||         Discover the hidden code!      ||");
            Console.WriteLine("||           Numpad1: select 1st #        ||");
            Console.WriteLine("||           Numpad2: select 2nd #        ||");
            Console.WriteLine("||           Numpad3: select 3rd #        ||");
            Console.WriteLine("||           Numpad4: select 4th #        ||");
            Console.WriteLine("||           Numpad5: select 5th #        ||");
            Console.WriteLine("||         Enter: check if correct        ||");
            Console.WriteLine("||  Bulls: Correct code, correct position ||");
            Console.WriteLine("||    Cows: Correct code, wrong position  ||");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.WriteLine("|| | ? | ? | ? | ? | ? |   Bulls   Cows   ||");
            Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
            Console.ResetColor();
            Console.WriteLine("|| | 0 | 0 | 0 | 0 | 0 |     0      0     ||");
        }

        //Genrate random secret number without duplicates
        public void generateSecretNum() { 
            secNum[0] = randNum.Next(0, 10);
            do {
                secNum[1] = randNum.Next(0, 10);
            } while (secNum[1] == secNum[0]);
            do {
                secNum[2] = randNum.Next(0, 10);
            } while (secNum[2] == secNum[1] || secNum[2] == secNum[0]);
            do {
                secNum[3] = randNum.Next(0, 10);
            } while (secNum[3] == secNum[2] || secNum[3] == secNum[1] || secNum[3] == secNum[0]);
            do {
                secNum[4] = randNum.Next(0, 10);
            } while (secNum[3] == secNum[2] || secNum[3] == secNum[1] || secNum[3] == secNum[0]);
        }

        //Increments guess numbers and checks if guess was correct
        public void getUserInput() {
            ConsoleKey key;
            do {
                // Key is available - read it.
                key = Console.ReadKey(true).Key;

                if (key == ConsoleKey.NumPad1) {
                    A++;
                    if (A > 9) A = 0;
                    Console.SetCursorPosition(0, lineNum);
                    Console.WriteLine("|| | " + A + " | " + B + " | " + C + " | " + D + " | " + E + " |     " + Bulls + " \t    " + Cows + " \t  ||");
                } else if (key == ConsoleKey.NumPad2) {
                    B++;
                    if (B > 9) B = 0;
                    Console.SetCursorPosition(0, lineNum);
                    Console.WriteLine("|| | " + A + " | " + B + " | " + C + " | " + D + " | " + E + " |     " + Bulls + " \t    " + Cows + " \t  ||");
                } else if (key == ConsoleKey.NumPad3) {
                    C++;
                    if (C > 9) C = 0;
                    Console.SetCursorPosition(0, lineNum);
                    Console.WriteLine("|| | " + A + " | " + B + " | " + C + " | " + D + " | " + E + " |     " + Bulls + " \t    " + Cows + " \t  ||");
                } else if (key == ConsoleKey.NumPad4) {
                    D++;
                    if (D > 9) D = 0;
                    Console.SetCursorPosition(0, lineNum);
                    Console.WriteLine("|| | " + A + " | " + B + " | " + C + " | " + D + " | " + E + " |     " + Bulls + " \t    " + Cows + " \t  ||");
                } else if (key == ConsoleKey.NumPad5) {
                    E++;
                    if (E > 9) E = 0;
                    Console.SetCursorPosition(0, lineNum);
                    Console.WriteLine("|| | " + A + " | " + B + " | " + C + " | " + D + " | " + E + " |     " + Bulls + " \t    " + Cows + " \t  ||");
                } else if (key == ConsoleKey.Enter) {
                    if (A == B || A == C || A == D
                     || B == A || B == C || B == D
                     || C == A || C == B || C == D
                     || D == A || D == B || D == C
                     || E == A || E == B || E == C || E == D) {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.SetCursorPosition(0, lineNum + 1);
                        Console.WriteLine("||~~~~ Duplicates!! ~~~~~~~~~~~~~~~~~~~~~~||");
                        Console.ResetColor();
                    } else {
                        checkBullsAndCows();
                        lineNum++;
                        Console.SetCursorPosition(0, lineNum);
                        Console.WriteLine("|| | " + A + " | " + B + " | " + C + " | " + D + " | " + E + " |     " + Bulls + " \t    " + Cows + " \t  ||");
                    }
                }

            } while (Bulls != 5);//Exit loop only when player wins the game.
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("||~~~~~ WELL DONE! YOU GUESSED IT!! ~~~~~~||");
            Console.ResetColor();
            endGame();
        }

        //Game logic, check how many bulls or cows
        public void checkBullsAndCows() {
            Bulls = secNum[0] == A ? bullA = 1 : bullA = 0;
            Bulls = secNum[1] == B ? bullB = 1 : bullB = 0;
            Bulls = secNum[2] == C ? bullC = 1 : bullC = 0;
            Bulls = secNum[3] == D ? bullD = 1 : bullD = 0;
            Bulls = secNum[4] == E ? bullE = 1 : bullE = 0;
            Bulls = bullA + bullB + bullC + bullD + bullE;

            Cows = (A == secNum[1] || A == secNum[2] && A != secNum[0]) ? cowA = 1 : cowA = 0;
            Cows = (B == secNum[0] || B == secNum[2] && B != secNum[1]) ? cowB = 1 : cowB = 0;
            Cows = (C == secNum[0] || C == secNum[1] && C != secNum[2]) ? cowC = 1 : cowC = 0;
            Cows = (D == secNum[0] || D == secNum[1] || D == secNum[2] && D != secNum[3]) ? cowD = 1 : cowD = 0;
            Cows = (E == secNum[0] || E == secNum[1] || E == secNum[2] || E == secNum[3] && E != secNum[4]) ? cowE = 1 : cowE = 0;
            Cows = cowA + cowB + cowC + cowD +cowE;
        }

        public void endGame() { //End game method
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            notGameOver = false;
        }
    }
}
