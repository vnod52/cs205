﻿using UnityEngine;

public static class ExtensionMethods {
    public static bool IsLessThan(this Vector2Int _a, Vector2Int _b) => (_a.x < _b.x || _a.y < _b.y);
    public static bool IsGreaterThan(this Vector2Int _a, Vector2Int _b) => (_a.x > _b.x || _a.y > _b.y);
    public static bool IsLessThanOrEqualTo(this Vector2Int _a, Vector2Int _b) => (_a.x > _b.x || _a.y > _b.y || _a.x == _b.x || _a.y == _b.y);
    public static bool IsGreaterThanOrEqualTo(this Vector2Int _a, Vector2Int _b) => (_a.x > _b.x || _a.y > _b.y || _a.x == _b.x || _a.y == _b.y);
}
