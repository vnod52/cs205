﻿using UnityEngine;

public class DraggableGameObject : MonoBehaviour {
    private Vector3 originalPosition;

    private void OnMouseDown() {
        originalPosition = transform.position - getMouseWorldPosition();
    }

    private Vector3 getMouseWorldPosition() {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Camera.main.WorldToScreenPoint(transform.position).z;
        return Camera.main.ScreenToWorldPoint(mousePosition);
    }

    private void OnMouseDrag() {
        transform.position = getMouseWorldPosition() + originalPosition;
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
