﻿using UnityEngine;

public class DraggableRigidBody : MonoBehaviour {
    public float force = 5;
    private Camera mainCamera;
    private Rigidbody selectedRigidBody;
    private Vector3 originalMousePosition;
    private Vector3 originalObjectPosition;
    private float selectionDistance;

    private void Awake() {
        mainCamera = Camera.main;
    }

    private void Update() {
        if (!mainCamera) {
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            selectedRigidBody = GetRigidBodyFromMouseClick();
        }

        if (Input.GetMouseButtonUp(0)) {
            selectedRigidBody = null;
        }
    }

    private Rigidbody GetRigidBodyFromMouseClick() {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit)) {
            Rigidbody selected = hit.collider.gameObject.GetComponent<Rigidbody>();
            if (selected) {
                selectionDistance = Vector3.Distance(ray.origin, hit.point);
                originalMousePosition = GetMouseWorldPosition();
                originalObjectPosition = hit.transform.position;

                return selected;
            }
        }
        return null;
    }

    private void FixedUpdate() {
        if (selectedRigidBody) {
            Vector3 mousePositionOffset = GetMouseWorldPosition() - originalMousePosition;
            selectedRigidBody.velocity = ((originalObjectPosition + mousePositionOffset) - selectedRigidBody.transform.position) * force;
        }
    }

    private Vector3 GetMouseWorldPosition() {
        Vector2 mousePostion = Input.mousePosition;
        return mainCamera.ScreenToWorldPoint(new Vector3(mousePostion.x, mousePostion.y, selectionDistance));
    }
}
