﻿
using UnityEngine;

public class RollyBall : MonoBehaviour {

    public float speed = 10;

    private Rigidbody rb;
    private Vector2 myInput;
    private float v;
    void Start() {
        rb = GetComponent<Rigidbody>();
    }

    void Update() {
        myInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void FixedUpdate() {
        rb.velocity = new Vector3(myInput.x * speed, rb.velocity.y, myInput.y * speed);
    }

    private void OnTriggerEnter(Collider other) {
        Destroy(other.gameObject);
        
    }
}
